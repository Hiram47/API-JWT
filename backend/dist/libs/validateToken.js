"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tokenValidation = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var tokenValidation = function tokenValidation(req, res, next) {
  var token = req.header('token');
  if (!token) return res.status(401).json({
    message: 'Access denied. You need a token to access'
  });

  var payload = _jsonwebtoken["default"].verify(token, process.env.TOKEN_SECRET || 'admindtys');

  console.log(payload); //req.userIdOrPassword=payload._id;    

  req.userIdOrPassword = payload._id;
  console.log(req.userIdOrPassword);
  next();
};

exports.tokenValidation = tokenValidation;