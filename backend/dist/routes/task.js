"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _task = require("../controllers/task");

var _validateToken = require("../libs/validateToken");

var router = (0, _express.Router)();
router.get('/tasks', _task.getTasks);
router.post('/tasks');
router.get('/task/:id', _validateToken.tokenValidation, _task.getTask);
router.get('/records/:start/:end', _validateToken.tokenValidation, _task.getRecords);
router.get('/recordsByArco/:start/:end/:farco', _validateToken.tokenValidation, _task.getRecordsByArco);
router.post('/signup', _task.signup);
router.post('/signin', _task.signin);
router.get('/antenas', _validateToken.tokenValidation, _task.getAntenas);
var _default = router;
exports["default"] = _default;