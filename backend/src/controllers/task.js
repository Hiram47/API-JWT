import { connect } from "../database"
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
//import exec from 'child_process'
import ping from 'ping'
import { reject } from "bcrypt/promises"


export const getTasks= async(req,res) =>{
    const db = await connect()
    try {
        const [rows]=await db.query('SELECT * FROM HISTORIAL_VEHICULOS hv INNER JOIN dispositivos d ON d.IP=hv.IP INNER JOIN vehiculos v ON v.IDCHIP=hv.IDCHIP LIMIT 10')
        console.log(rows);
        res.json(rows)
        
    } catch (error) {

        res.json(`${error} error en consulta`)

        
    }

}
export const getArcos = async(req,res)=>{
    const db = await connect()
    try {
        const [rows]=await db.query('SELECT * FROM arcos')

        console.log(rows);
        
        res.json(rows)
        
    } catch (error) {
        res.json(`${error} error en consulta`)
        
    }
    
}
//new
export const getAntenas = async(req,res)=>{
    const db=await connect();
    try {
       const [rows]= await db.query( `SELECT * FROM dispositivos`);
       console.log(rows);
       res.json(rows);
        
    } catch (error) {
        console.log(error);
        res.json(`${error} error en consulta`)
        
    }

}

// export const getPing= async(req,res)=>{
//     const db=await connect();
//     const isAvailable = false;
//     const arcos=[];
//     let arcos2;
    
//     try {
//        let [rows]= await db.query( `SELECT IP FROM dispositivos`);
//        console.log(rows);
//        const ip= rows.forEach(async (item)=>{

//        // arcos.push(item.IP)
//         console.log(item.IP);
//       //  exec(`ping -c 3 ${item.IP}`,process)
//       const answ=   ping.promise.probe(item.IP)
//     .then(function(respuesta){
//       //  console.log(res);
//         if(respuesta.host){
//             console.log(respuesta.host,respuesta.alive);
//         //   arcos=JSON.stringify(respuesta.host,respuesta.alive)
//           // res.send(respuesta.host,respuesta.alive);
//         }
//     })

    
        
//    console.log({host:answ.host, alive:answ.alive });
 
//      arcos.push( {host:answ.host, alive:answ.alive })
//    // arcos2+=answ.host;
//     //console.log(arcos2);

//     //res.status(200).send(answ.host,answ.alive)
    
//     // console.log(answ.host);
    
//     //answ.then(function)
    
// })


// //return res.send(arcos)
// //console.log(ip);

// //hola
// //console.log(arcos);

// //console.log(answ.alive);
// // const arcos2 = async() =>{
//   //  arcos=answ
// //     const a =await answ;
// //     console.log(a);
// // }
// setInterval(() => {
    
//     res.json(arcos);
   
// }, 10000);
        
//     } catch (error) {
//         console.log(error);
//       //  res.json(`${error} error en consulta`)
        
//     }


// }

//

//TEMPORAL

export const getPing = async (req, res) => {
    const db = await connect();
    
 //   let arcos = [];
    try {
        let arcos = [];
        const [rows, metadata] = await db.query(`SELECT IP,NOMBRE FROM dispositivos`)

        for await (const row of rows) {

            const { numeric_host, alive } = await ping.promise.probe(row.IP)
            //@ts-ignore 
                   
            arcos.push({ numeric_host, alive,nombre:row.NOMBRE})
            
        }
        console.log('arcos ->', arcos);
        res.json(arcos)

    } catch (error) {
        console.log(error);
        res.json(`${error} error en consulta`)

    }
}

//
/// AGRUPADO POR ARCO NEW
        export const getCountByArco=async(req,res)=>{
            console.log(req.params.start,req.params.end);
            let start= req.params.start;
            let end= req.params.end;
            const db = await connect()
        
            try {
                const [rows]= await db.query(`SELECT COUNT(*) AS lectura, NOMBRE FROM  HISTORIAL_VEHICULOS v INNER JOIN dispositivos d ON d.IP=v.IP WHERE FECHA_LECTURA BETWEEN ? AND ? GROUP BY v.IP `,[start,end])
                res.json(rows)
            } catch (error) {
                res.json(`${error} error en consulta`)
                
            }
        
        

            


        }
///


export const getTask = async(req,res) => {

    console.log(req.params.id);
    let id = req.params.id
    const db = await connect()
    try {
        //SELECT * FROM HISTORIAL_VEHICULOS hv INNER JOIN dispositivos d ON d.IP=hv.IP INNER JOIN vehiculos v ON v.IDCHIP=hv.IDCHIP WHERE hv.IDCHIP=? or v.VIN=?
        const [rows]= await db.query(`SELECT * FROM HISTORIAL_VEHICULOS hv LEFT JOIN dispositivos d ON d.IP=hv.IP  WHERE hv.IDCHIP=? order by hv.FECHA_LECTURA desc`,[id])
        res.json(rows)
    } catch (error) {
        res.json(`${error} error en consulta`)
    }
    

}

export const getRecords =async(req,res)=>{
    console.log(req.params.start,req.params.end);
    let start= req.params.start;
    let end= req.params.end;
    const db = await connect()

    try {
        const [rows]= await db.query(`SELECT COUNT(*) as counting FROM HISTORIAL_VEHICULOS where FECHA_LECTURA BETWEEN ? AND ?`,[start,end])
        res.json(rows[0])
    } catch (error) {
        res.json(`${error} error en consulta`)
        
    }


}

export const getRecordsByArco =async(req,res)=>{
    console.log(req.params.start,req.params.end,req.params.farco);
    let start= req.params.start;
    let end= req.params.end;
    let arco=req.params.farco
    const db = await connect()

    try {
        const [rows]= await db.query(`SELECT COUNT(*) as counting FROM HISTORIAL_VEHICULOS where FECHA_LECTURA BETWEEN ? AND ? AND IP=?`,[start,end,arco])
        res.json(rows[0])
    } catch (error) {
        res.json(`${error} error en consulta`)
        
    }


}


export const signup = async(req,res)=>{
    const {NOMBRE,APELLIDO,USUARIO,PASSWORD}=req.body
    const hashed = await bcrypt.hash(PASSWORD,10)

    
    try {
        const db = await connect()
        
        const [result] = await db.query("INSERT INTO `usuarios`(`NOMBRE`,`APELLIDO`,`USUARIO`,`PASSWORD`) VALUES (?,?,?,?)",[NOMBRE,APELLIDO,USUARIO,hashed])   
        console.log(result);

        const token= jwt.sign({_id:req.body.USUARIO}, process.env.TOKEN_SECRET || 'admindtys')
        console.log(req.body);
        console.log(token);
        res.status(200).json({token})  
    } catch (error) {

        console.log(error);

        res.json({error});
        
    }





}

export const signin = async(req,res) =>{

    const {NOMBRE,APELLIDO,USUARIO,PASSWORD}=req.body
    const hashed = await bcrypt.hash(PASSWORD,10)

    
    
    try {
        const db = await connect()
        const [rows] = await db.query(`SELECT * from usuarios WHERE USUARIO =?`,[USUARIO])
        console.log(rows[0].PASSWORD);
        console.log(PASSWORD);
        const isEqual = await bcrypt.compare(PASSWORD,rows[0].PASSWORD)
      //  console.log(isEqual+'d');
      if (isEqual) {

        const token = jwt.sign({_id:USUARIO},process.env.TOKEN_SECRET||'admindtys')

        console.log(hashed);
        console.log(isEqual);
        res.status(200).json({token,ok:true,username:rows[0].USUARIO,uid:rows[0].ID})
          
      }


    } catch (error) {

        res.json(error)
        
    }


   // res.status(200).json({token})
    // res.json({PASSWORD})




}