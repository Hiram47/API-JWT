import jwt from 'jsonwebtoken'


export const tokenValidation=(req,res,next)=>{

    const token = req.header('token')

    if(!token) return res.status(401).json({message:'Access denied. You need a token to access'})

    const payload= jwt.verify(token,process.env.TOKEN_SECRET|| 'admindtys') 
    console.log(payload);

    req.userIdOrPassword=payload._id;    

    next();
}