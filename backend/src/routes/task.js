import { Router } from "express";
import { getAntenas, getArcos, getCountByArco, getPing, getRecords, getRecordsByArco, getTask, getTasks, signin, signup } from "../controllers/task";
//import { tokenValidation } from "../libs/validateToken";
import {tokenValidation} from '../libs/validateToken'



const router = Router()

router.get('/tasks',getTasks)

router.post('/tasks')

router.get('/arcos',getArcos)

router.get('/task/:id',tokenValidation,getTask)

router.get('/records/:start/:end',tokenValidation,getRecords)

router.get('/recordsByArco/:start/:end/:farco',tokenValidation,getRecordsByArco)

router.post('/signup',signup)

router .post('/signin',signin)

router.get('/antenas',tokenValidation,getAntenas)

router.get('/ping',getPing)

router.get('/countByArco/:start/:end',getCountByArco)

export default router;