import { useDispatch, useSelector } from "react-redux"
import { repuveApi } from "../api"
import { ClearError, onChecking, onLogin, onLogout } from "../store"



export const useAuthStore = () =>{

    const {status,user,errorMessage} = useSelector(state => state.auth)

    const dispatch = useDispatch()


    const startLogin = async({USUARIO,PASSWORD})=>{
        console.log({USUARIO,PASSWORD});
        dispatch(onChecking())
        try {
            const {data}= await repuveApi.post('/signin',{USUARIO,PASSWORD})
          
            console.log(data);
            localStorage.setItem('token',data.token)
            console.log('iniciando....');
            localStorage.setItem('user',JSON.stringify(data))
          //  console.log(localStorage.getItem('token'));

            localStorage.setItem('token-init-date',new Date().getDate())

            console.log(data);
            if (data.token) {
                
                dispatch(onLogin({username:data.username,uid:data.uid}))
            }else{
                dispatch(onLogout('Credenciales incorrectas'))
                setTimeout(() => {
                    dispatch(ClearError())
                    
                }, 50);
            }

            
            // if (resp.data ) {
            //     console.log(resp.data);
                
            // }else {
            //     console.log('Error en usuraio y/o contraseña');
            // }
        } catch (error) {
            console.log(error);
            
        }
    }

    const startregister = async({USUARIO,PASSWORD,NOMBRE,APELLIDO})=>{
        console.log({USUARIO,PASSWORD,NOMBRE,APELLIDO});
        dispatch(onChecking())
        try {
            const {data}= await repuveApi.post('/signup',{USUARIO,PASSWORD,NOMBRE,APELLIDO}
           // ,{headers:{'token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJuZW8iLCJpYXQiOjE2NTk2NDM2MTJ9.sdZLxTJknzmpHog3GgqWpftoBOyjJ3yQ6MYc5eyV6lc'}}
            )
            localStorage.setItem('token',data.token)
            localStorage.setItem('user',JSON.stringify(data))
            localStorage.setItem('token-init-date',new Date().getDate())

            if (data.token) {
                
                dispatch(onLogin({username:data.username,uid:data.uid}))
            }else{
                dispatch(onLogout('Credenciales incorrectas'))
                setTimeout(() => {
                    dispatch(ClearError())
                    
                }, 50);
            }

        } catch (error) {
            console.log(error);
            
        }
    }
    ////

    const checkAuthToken = async() =>{

        
        const token = localStorage.getItem('token')
        if (!token) {
            return dispatch(onLogout())
        }
        try {
         
            console.log(JSON.parse(localStorage.getItem('user')));
            const ldata=JSON.parse(localStorage.getItem('user'))
            dispatch(onLogin({username:ldata.username,uid:ldata.uid}))
            console.log('CHECANDO');
        } catch (error) {
            console.log(error);

           localStorage.clear()
           dispatch(onLogout())
            
        }
    }

    const startLogout=()=>{
        localStorage.clear();
        dispatch(onLogout())
    }

    return {
        //prierties
        status,user,errorMessage,

        //methods
        startLogin,
        startregister,
        checkAuthToken,
        startLogout
    }
}