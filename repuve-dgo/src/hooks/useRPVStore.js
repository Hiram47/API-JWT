import { useDispatch } from "react-redux";
import { repuveApi } from "../api";
import { errorRepuveMessage, information ,loadInfo} from "../store/repuve/repuveSlice";
import { records,load, arcos } from "../store/repuve/repuveArcosSlice";


export const useRPVStore=() =>{

    const dispatch=useDispatch()

    const searchVin= async({vin})=>{
        console.log(vin);
        dispatch(loadInfo())
        try {
           const {data}= await repuveApi.get(`/task/${vin.pvin}`,{headers:{'token':localStorage.getItem('token')}})
           console.log(data);
           dispatch(information(data))
            
        } catch (error) {
            dispatch(errorRepuveMessage(error.message))
            console.log(error);            
        }

    }

    const searchDate= async({start,end})=>{

        console.log(start,end);
        dispatch(load())
        try {
            const {data}= await repuveApi.get(`/records/${start}/${end}`,{headers:{'token':'$2b$10$WLSohv8ghP9C/ddF7CWlt.VQNPBcRzeFTq64nQHMV/bMv4my08SgO'}})

            console.log(data);
            
            dispatch(records(data))

        } catch (error) {
            console.log(error);
            
        }

    }

    const getStoreAntenas= async()=>{
        // dispatch(load())

        try {
            const {data}= await repuveApi.get(`/antenas`,{headers:{'token':localStorage.getItem('token')}})
            console.log(data);
            dispatch(arcos(data))

            
        } catch (error) {
            console.log(error);
            
        }

    }

    const searchDatesByRecords= async({farcos,finicio,ffin})=>{
        dispatch(load())
        try {

            console.log(farcos,finicio,ffin);
            const {data}= await repuveApi.get(`/recordsByArco/${finicio}/${ffin}/${farcos}`,{headers:{'token':localStorage.getItem('token')}})
            console.log(data);
            dispatch(records(data))

        
        } catch (error) {
            console.log(error);
            
        }
    }

    return{
        searchVin,
        searchDate,
        getStoreAntenas,
        searchDatesByRecords

    }
}