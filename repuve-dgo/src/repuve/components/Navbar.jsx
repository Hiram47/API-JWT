import React from 'react'
import { Link } from 'react-router-dom'
import { useAuthStore } from '../../hooks'

export const Navbar = () => {

  const { startLogout,user }= useAuthStore()
  
    let usuario=user.username



  

  
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark mb-4 px-4'>
        <span className='navbar-brand'>
            <i></i>
           {            
              usuario?.toUpperCase()                       
           } 

        </span>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav">
      <li className="nav-item active">
        <Link className="nav-link" to='/'>Inicio <span className="sr-only"></span></Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to='/arcos'>Arcos</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to='/map'>Mapa</Link>
      </li>
      {/* <li className="nav-item">
        <a className="nav-link" href="#">Pricing</a>
      </li>
      <li className="nav-item">
        <a className="nav-link disabled" href="#">Disabled</a>
      </li> */}
    </ul>
  </div>
 
        

        <button className='btn btn-outline-danger' onClick={startLogout}>
          Logout
        </button>
        



    </nav>
  )
}
