import React, { useEffect, useLayoutEffect } from 'react'
import { useState } from 'react'
import { useSelector } from 'react-redux'
import Swal from 'sweetalert2'
import { repuveApi } from '../../api'
import { useRPVStore } from '../../hooks'
import { Navbar } from '../components/Navbar'



export const ArcosPage = () => {
    const [dates, setdates] = useState({start:'',end:''})
    const initialState={
        start:undefined,
        end:undefined
    }
    const {start,end}=initialState

    const [bow,setbow]= useState({finicio:'',ffin:'',farcos:''});

    const [anthenas,setanthenas]=useState([])
    const repuve= useSelector(state => state.repuveArcos)
    console.log(repuve);
    console.log(repuve.antenas);

    const antenaComponent= repuve.antenas.map(a=>{
     return <option key={a.ID} value={a.IP}>{a.NOMBRE.toUpperCase()}</option>
  
    })
  
    
    const {searchDate,getStoreAntenas,searchDatesByRecords}=useRPVStore()   
    
    const antenas=async () =>{
        const antena =await getStoreAntenas()
        console.log(antena);
        //setanthenas(antena)

    }
    useEffect(()=>{
        console.log(antenas());

    },[])

 


    ///////////////////////////////////////
    // const getStoreAntena= async()=>{

    //     try {
    //         const {data}= await repuveApi.get(`/antenas`,{headers:{'token':localStorage.getItem('token')}})
    //         console.log(data);
           
    //         setanthenas(data)
          

            
    //     } catch (error) {
    //         console.log(error);
            
    //     }

    // }
    // useEffect(()=>{
    //     (async() =>{

    //     const ant = await getStoreAntena()
      

    // })()

    // },[])

 

    
   
    ////////////////////////////////////////////
    // useEffect(()=>{
        
       
    //    //  console.log(antenas());

    // (async() =>{

    //     const ant = await getStoreAntenas()
    //   //  setanthenas(ant)
    //     console.log(await getStoreAntenas());

    // })()
     

    // },[])

    
    
   

    const inputChangeArco=(e)=>{
        console.log({...bow,[e.target.name]:e.target.value});
        setbow({...bow,[e.target.name]:e.target.value});

    }


    

    const inputChange=(e)=>{
        console.log({...dates,[e.target.name]:e.target.value});
        setdates({...dates,[e.target.name]:e.target.value});
    }

    const submitArcoChange=(e)=>{
        e.preventDefault();
     //   console.log(bow);
        if (bow.finicio===''|| bow.ffin===''||bow.farcos==='') {
            console.log('Empty fields');   
            Swal.fire({
                title:'Campos vacios',
                text:'Es necesario llenar todos los campos',
                timer:3500,
                icon:'error',
                background:'black',
                color:'white',
                footer:'DTYS corps'
            })         
        }
        else{
            if (bow.finicio>bow.ffin) {
                Swal.fire({
                    title:'Fechas incorrecta',
                    text:'Ingresar fechas correctas',
                    timer:3000,
                    icon:'error',
                    background:'black',
                    color:'white',
                    footer:'DTYS corps',
                    
                })


                
            }else{
                setbow(bow)
                searchDatesByRecords(bow)
                console.log(bow,'sending...');

            }

        }

    }

    const submitchange=(e)=>{
       e.preventDefault()
    //   setdates(dates)
     //  console.log(dates,'sending...');

       if (dates.start==='' || dates.end==='') {
              console.log('campos vacios');
              Swal.fire({
                title:'Campos vacios',
                text:'Es necesario llenar todos los campos',
                timer:3500,
                icon:'error',
                background:'black',
                color:'white',
                footer:'DTYS corps'
            })
            //  Swal.fire('Campos vacios','Es necesario llenar todos los campos','error')
        
       }else{

            if (dates.start>dates.end) {
                console.log('fecha inicial es mayor que fecha final');
                //alert('fecha inicial es mayor que fecha final');
                Swal.fire({
                    title:'Fechas incorrecta',
                    text:'Ingresar fechas correctas',
                    timer:3000,
                    icon:'error',
                    background:'black',
                    color:'white',
                    footer:'DTYS corps',
                    
                })
                
            }else{

                  setdates(dates)
                  searchDate(dates)
                  console.log(dates,'sending...');
            }

       }

    }

    useEffect(()=>{

           if (repuve.loading==false) {
               
               console.log(antenas());
               console.log('arcos');
           }

         //console.log(antenaComponent);
   
       },[bow.finicio,bow.ffin])



  return (
    <>
        <Navbar/>
        <div className='container text-white'>
            <h2>Arcos de repuve</h2>
            <hr />
            <form onSubmit={submitchange} className='row offset-4'>
                <div className='col'>
                    <label htmlFor="">Fecha inicio</label>
                    <input className='form-control ' name='start' type="date" placeholder='' onChange={inputChange} />
                </div>
                <div className='col'>
                    <label htmlFor="">Fecha final</label>
                    <input className='form-control' name='end' type="date" placeholder='' onChange={inputChange} />

                </div>
                <div className='col'>
                    <label htmlFor=""> </label>
                    <button type='submit' className='btn btn-dark mt-4'>
                        Buscar
                    </button>

                </div>

            </form>

            <hr />

            <form onSubmit={submitArcoChange} className='row mt-4 offset-4'>
                <div className='col-md-3'>
                    <label htmlFor="">fecha inicio</label>
                    <input type="date" className='form-control' name='finicio' onChange={inputChangeArco} />

                </div>
                <div className='col-md-3'>
                    <label htmlFor="">fecha final</label>
                    <input type="date" className='form-control' name='ffin' onChange={inputChangeArco} />

                </div>
                <div className='col-md-3'>

                <label htmlFor="">Dispositivo</label>
                <select  name="farcos"  className='form-control ' onChange={inputChangeArco}>
               {
                    // repuve.antenas.map(a =>{
                    // return    <option value={a.IP}>{a.NOMBRE?.toUpperCase()}</option>
                    //     console.log(a.IP)
                    // })
                    antenaComponent
                    
                }
                </select>
                </div>
                <div className='col-md-3'>
                    <button className='btn btn-dark mt-4'>Buscar</button>

                </div>

            </form>

            <hr />

            {

                repuve.loading==false ?
                 <h3>Lecturas: {repuve.data.counting?.toLocaleString(undefined)}</h3> : 
                    <div className='text-center mt-3'>

                    <div className='spinner-border' role='status'>
                            <span className='sr-only'></span>

                            {/* <h3>Loading...</h3> */}

                        </div>
                </div>

            }




        </div>
        
        </>
  )
}
