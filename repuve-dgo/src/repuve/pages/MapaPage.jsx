import React, { useEffect, useMemo, useRef, useState } from 'react'
import { Navbar } from '../components/Navbar'

import {MapContainer,TileLayer,useMap,Marker,Popup,CircleMarker} from 'react-leaflet'
 import iconMarker from 'leaflet/dist/images/marker-icon.png'
 import 'leaflet/dist/leaflet.css'

 import PrintControl from 'leaflet-easyprint';

 import { Icon } from 'leaflet'
import { getEnvVariables } from '../../helpers'



export const MapaPage = () => {

    const {VITE_API_URL}=getEnvVariables()

    // const PrintControl = withLeaflet(PrintControlDefault);
     const icon = L.icon({ iconUrl: iconMarker });

    //  const printControlRef = useRef()

     const coords=[
        [24.024117,-104.552357,'unipoli'],
        [24.03415,-104.64697,'ITD'],
        [23.98991,-104.61928,'UTD'],
        [24.03992,-104.65792,'carnation'],
        [23.99496,-104.66299,'sep'],
        [24.03128,-104.68569,'armando del castillo'],
        [24.02620,-104.66186,'av. 20 noviembre'],
       // [25.55239,-103.48853,'martins GP'],// no estoy seguro de la ubicacion,
       [25.54631,-103.46990,'blvd miguel aleman'],
       [25.52750,-103.53006,'blvd ejercito mexicano'],
       [25.60779,-103.51060,'jimenez puente peatonal'],
       [24.00373,-104.69735,'carretera mazatlan']

     ]

     const [locations, setlocations] = useState([])

     //new
     const [arcoPing, setarcoPing] = useState([])

     const [statusPing, setstatusPing] = useState(false)

     

    //  useEffect(() => {

    //     const fetchPing = async() =>{
    //         const resp = await fetch(`${VITE_API_URL}/ping`)
    //         const info = await resp.json()
    //         console.log(info);
    //         setarcoPing(info)

    //     }
    //     fetchPing()

    
    //  }, [])
     
     //

     useEffect(() => {

        const fetchArcos = async()=>{
            const res = await fetch( `http:localhost:3100/arcos`)
            const data = await res.json()
            setlocations(data)


        }
        fetchArcos()
    
     }, [])
     
  return (
    <>
    
    <div >

        <Navbar/>
        <div className=' text-white'>

            <h3 className='text-center'>Mapa de ubicaciones</h3>
            <hr />

            <div className='row'  
            // style={{'width':'350px','height':'200px'}}
            >
                <div className='col-md-3 m-2 '>
                    <ul>
                        {/* <li><small>UNIPOLI</small></li>
                        <li><small>UTD</small></li> */}
                        {
                            locations.map((locate,index)=>{
                               return <li  key={locate.id}>{locate.id} {locate.nombre.toUpperCase() } <span 
                               className={ locate.status===1 ? "badge rounded-pill text-bg-success p-2" : "badge rounded-pill text-bg-danger p-2"}
                       
                                // style={locate.status===1?{color:'green'}:{color:'red'} }
                                >  &nbsp; &nbsp; </span></li>
                            })
                            // !arcoPing?<h2>Cargando...</h2>:
                            // arcoPing.map((ping,index)=>{
                            //     return <li key={index+1} className={`badge rounded-pill ${ping.alive?'text-bg-success':'text-bg-danger'} `}>{index+1} {ping.nombre?.toUpperCase()}  <span
                            //    // className={ping.alive ? "badge rounded-pill text-bg-success p-2":"badge rounded-pill text-bg-danger p-2"}
                            //    // className={`badge rounded-pill ${ping.alive?'text-bg-success':'text-bg-danger'} p-2`}
                            //     >
                            //         </span>&nbsp; &nbsp;</li>
                            //     })
                        }
                    </ul>

                </div>
                <div className='col'>


                <MapContainer 
                // position={[51.505, -0.09]} 
                style={{ height: "80vh", width: "70vw" }}
                 center={[24.02163, -104.53752]}
                 zoom={13} 
                 scrollWheelZoom={true}
                 >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {
                        locations.map((cord,i)=>{

                            console.log(cord);

                   return <Marker icon={icon} position={[cord.lat,cord.long]}>
                        <Popup>
                        {/* 
                             A pretty CSS3 popup. <br /> Easily customizable. 
                        */}
                        {
                            <h1>N°{i+1} {cord.nombre?.toUpperCase()}</h1>
                        }
                        </Popup>
                    </Marker>
                        })
                    }
                    ///////
                    {/* <PrintControl ref={printControlRef} position="topleft" sizeModes={['Current', 'A4Portrait', 'A4Landscape']} hideControlContainer={false} /> */}
                    {/* <PrintControl position="topleft" sizeModes={['Current', 'A4Portrait', 'A4Landscape']} hideControlContainer={false} title="Export as PNG" exportOnly /> */}



                </MapContainer>
                </div>
            </div>

                {
                    // coords.map(cord =>{
                    //  return   <h4>{cord[0]} {cord[1]}</h4>
                    // })
                }
        </div>
    </div>
    </>
  )
}
