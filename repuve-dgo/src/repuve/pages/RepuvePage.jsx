import React from 'react'
import { useState } from 'react'
import { Navbar } from "../components/Navbar"
import './RepuvePage.css'
// import {searchvin} from '../../hooks/useRepuveStore'
//import  {searchvin}   from '../../hooks';

import { useRPVStore} from '../../hooks'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import moment from 'moment'
import Swal from 'sweetalert2'





export const RepuvePage = () => {

  //const {startLogin,errorMessage,startregister}=useAuthStore()
  
  const selector=useSelector(state=> state.repuve)
  // useEffect(() => {
  //   if (selector.loading) {
      
  //     console.log('cargando');
  //   }
  
  // }, [selector.loading])
  

  const {searchVin}=useRPVStore()
  
  const initialState={
    pvin:''
  }

  const {pvin}=initialState
  const [vin, setvin] = useState(initialState)

  const inputChange=(e)=>{

    console.log({...vin,[e.target.name]:e.target.value});
    setvin({[e.target.name]:e.target.value})

  }

  const submitChange=(e)=>{
    e.preventDefault()

    console.log(vin,'enviado');
    setvin(vin)
    searchVin({vin})
    console.log(selector);
   // searchvin(pvin)
   


  }

  return (
    <>
        <Navbar/>

        <div className='container text-white'>

          <h3>Consulta</h3>
          <hr />

          <form onSubmit={submitChange} className='offset-4'>

          <label htmlFor="">VIN/PLACA</label>
          <input type="text" name='pvin' className='form-control w-50' placeholder='VIN' onChange={inputChange}/>

            <button className='btn btn-secondary mt-3 w-50'>Search</button>

          </form>
          <table className="table mt-3">
  <thead className="table-dark text-white disable-text-selection" >
      <tr>
                <th>#</th>
            <th>IDCHIP</th>
            <th>UBICACION</th>
            <th>NOMBRE</th>
            <th>IP</th>
            <th>LECTURA</th>
            {/* <th>Municipio</th>
            <th>Marca</th>
            <th>Version</th>
            <th>Modelo</th>
            <th>Serie</th>
            <th>Color</th> */}
            
      </tr>
    
  </thead>
  <tbody className='text-white disable-text-selection'>
     
  {
    
  selector.errorMessage
  ?
//   Swal.fire({
//     title:'Campo vacio',
//     text:'d',
//     timer:3000,
//     icon:'error',
//     background:'black',
//     color:'white',
//     footer:'DTYS corps',
    
// })
    //  <h3>{selector.errorMessage}</h3>
    <h3>No ingreso vin o placa</h3>
  :
          selector.loading==false
          ?    
              selector.data.map((task,i) =>{
              return <tr key={task.ID_HISTORIAL}>
                  <td>{i+1}</td>
                    <td>{task.IDCHIP}</td>
                    <td>{task.UBICACION}</td>
                    <td>{task.NOMBRE}</td>
                    <td>{(task.IP)?task.IP:'Desconocido'}</td>
                    {/* moment(food.fecha).format('DD/MM/YYYY')  */}
                    <td>{moment(task.FECHA_LECTURA).format('DD/MM/YYYY')}</td>       
              </tr>     
            })
        :
            <div className='col-md-12 text-center mt-3'>

              <div className='spinner-border text-center' role='status'>
                <span className='sr-only'></span>
                  {/* <h3>Loading...</h3> */}
              </div>
            </div>

  }
  </tbody>
</table>

         


        </div>
        
    </>
  )
}
