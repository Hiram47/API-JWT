import { useEffect } from 'react'
import {Navigate, Route, Routes} from 'react-router-dom'
import { LoginPage } from '../auth'
import { getEnvVariables } from '../helpers'
import { useAuthStore } from '../hooks'
import { ArcosPage } from '../repuve/pages/ArcosPage'
import { MapaPage } from '../repuve/pages/MapaPage'
import { RepuvePage } from '../repuve/pages/RepuvePage'

 //import { RepuvePage } from '../repuve'



export const AppRouter = () => {

   // const authStatus='not-authenticated'
   const {status,checkAuthToken}=useAuthStore()

   console.log(status);

  //  if (status=== 'checking') {
  //    return (
  //      <h3>Loading...</h3>
  //    )
     
  //  }

  useEffect(() => {
  checkAuthToken()
  }, [])
  

    console.log(getEnvVariables());
  return (
    <Routes>

        {
            (status==='not-authenticated')
            ? (
              <>
               <Route path='/auth/*' element={<LoginPage/>}/>
               <Route path='/*' element={<Navigate to='/auth/login'/>}/>
              </>
               )
            :(
              <>
              
              <Route exact path="/" element={<RepuvePage/>}/>
              <Route exact path='/*' element={<Navigate to='/'/>}/>

              <Route path='/arcos' element={<ArcosPage/>}/>
              <Route path='/arcos' element={<Navigate to='/arcos'/>}/>

              <Route path='/map' element={<MapaPage/>}/>
              <Route path='/map' element={<Navigate to='/map'/>}/>
              </>
                
                )
        }
        

    </Routes>
  )
}
