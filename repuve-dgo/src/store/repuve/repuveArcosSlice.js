import { createSlice } from '@reduxjs/toolkit';

export const repuveArcosSlice = createSlice({
name: 'arcos',
    initialState: {
        data:{},
        loading:false,
        antenas:[]
        
    },
reducers: {
    records: (state,  action  ) => {

         state.data =action.payload;
         state.loading=false
         state.antenas=[]
    },
    load:(state)=>{

        state.data={}
        state.loading=true
        state.antenas=[]

    },
    arcos:(state,{payload})=>{
        state.data ={}
        state.loading=false
        state.antenas=payload

    }
}
});

export const { records ,load,arcos} = repuveArcosSlice.actions;