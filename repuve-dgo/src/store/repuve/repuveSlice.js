import { createSlice } from '@reduxjs/toolkit';

export const repuveSlice = createSlice({
name: 'repuve',
    initialState: {
        data:[],
        loading:false,
        errorMessage:undefined
    
    },
reducers: {
    information: (state,  action  ) => {

         state.data =action.payload;
         state.loading=false
         state.errorMessage=undefined
    },
    loadInfo:(state)=>{

        state.data=[]
        state.loading=true
        state.errorMessage=undefined

    },
     errorRepuveMessage:(state,{payload})=>{
        state.data=[]
        state.loading=false
        state.errorMessage=payload

     }
}
});

export const { information ,loadInfo,errorRepuveMessage} = repuveSlice.actions;