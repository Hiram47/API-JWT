import  { configureStore } from '@reduxjs/toolkit'
import { authSlice, uiSlice } from './'
import { repuveArcosSlice } from './repuve/repuveArcosSlice'
import { repuveSlice } from './repuve/repuveSlice'


export const store = configureStore({
    reducer:{
        auth:authSlice.reducer,
        ui:uiSlice.reducer,
        repuve:repuveSlice.reducer,
        repuveArcos:repuveArcosSlice.reducer
    }
})